<div align='center'>
  
# Soul 🍚 
  
![OS](https://img.shields.io/badge/Unstable-%23E0234E.svg?style=for-the-badge&logo=arch&logoColor=white&style=flat)
![GitHub](https://img.shields.io/github/license/HBlanqueto/dotfiles?label=Licence&logo=GNU&logoColor=ffffff&style=flat)
![GitHub last commit](https://img.shields.io/github/last-commit/HBlanqueto/dotfiles?label=Last%20commit&logo=GitHub)
![GitHub repo size](https://img.shields.io/github/repo-size/soulryuu/archinst?label=Repo%20size)

</div>

**Humberto's configuration files :octocat:**

Welcome to my system configuration files! My system is [Debian](https://wiki.debian.org/DebianUnstable) you will find my daily use programs & I'll invite you to read my [wiki](https://github.com/HBlanqueto/dotfiles/wiki). The awesomeWM configuration only works with [GitHub's repository](https://github.com/awesomeWM/awesome) version, you have to install `awesome-git` package in Arch Linux, if you are in another GNU/Linux distribution you have to build it.

**Wiki**

- [Home](https://github.com/HBlanqueto/dotsbian/wiki)
  - [Building Kernel](https://github.com/HBlanqueto/dotsbian/wiki/Building-Kernel)
  - [Flatpak](https://github.com/HBlanqueto/dotsbian/wiki/Flatpak)
  - [Minimal Gnome](https://github.com/HBlanqueto/dotsbian/wiki/Minimal-Gnome)
  - [Wayland](https://github.com/HBlanqueto/dotsbian/wiki/Wayland)

## System Info ℹ️
- **OS:** [Debian Unstable](https://wiki.debian.org/DebianUnstable) :trollface: 🧠
- **Terminal:** Wezterm & Foot 📟
- **Telegram Group:** [*Nix Lovers](https://t.me/unixlovers)
- **Shell:** Zsh 🐚🧙🏼‍♂️
- **Browser:** Firefox 🦊
- **Window Manager:** [awesome-git](https://github.com/awesomeWM/awesome) 🪐
- **Desktop Enviroment:** Gnome 40.5 wayland session 👣
- **Text Editor:** Neovim, nano 📝
- **Packages:** Apt & Flatpak 📦
- **File Manager:** Thunar & Nautilus 📁
- **Music:** Lollypop & ncmpcpp 🎧
- **Cursor:** [McMojave-cursor](https://github.com/vinceliuice/McMojave-cursors)

## Setup 🛠️ 
This repositorie has [submodules](https://github.blog/2016-02-01-working-with-submodules/), this is the correct way to clone it:
```bash
git clone --recurse-submodules https://github.com/HBlanqueto/dotfiles.git --depth 1
cd dotfiles
git submodule update --remote --merge
```

##
<div align='center'>
  
Special thanks to [unixporn community](https://www.reddit.com/r/unixporn/), most part of my dots are based on them.
  
</div>                 
